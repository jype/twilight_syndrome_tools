#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	uint16_t sector_offset;
	uint16_t sector_size;
} cdb_entry;

void *read_file(const char *file, size_t *size)
{
	FILE *f;
	void *data;
	long len;

	f = fopen(file, "rb");
	assert(f != NULL);

	assert(!fseek(f, 0, SEEK_END));
	len = ftell(f);
	assert(len >= 0);
	assert(!fseek(f, 0, SEEK_SET));

	data = malloc(len);
	assert(data != NULL);

	assert(fread(data, len, 1, f) == 1);
	assert(!fclose(f));

	if (size != NULL) {
		*size = len;
	}

	return data;
}

void cdb_decompress(uint8_t *inp, uint8_t *outp, size_t *ilen, size_t *olen)
{
	uint8_t val2;
	uint8_t *outptr;
	uint8_t *inptr;
	int32_t i;
	uint32_t len;
	uint8_t val1;
	uint8_t *old_inp;
	uint8_t *old_outp;

	old_inp = inp;
	old_outp = outp;

	do {
		len = (uint32_t)*inp;
		inptr = inp + 1;
		if (len < 0x80) {
			len = len + 1;
			i = 0;
			if (len != 0) {
				do {
					val1 = *inptr;
					inptr = inptr + 1;
					i = i + 1;
					*outp = val1;
					outp = outp + 1;
				} while (i < (int32_t)len);
			}
		}
		else if (len < 0xc0) {
			len = len - 0x7d;
			if (*inptr == 0) {
				i = 0;
				if (0 < (int32_t)len) {
					do {
						*outp = 0;
						i = i + 1;
						outp = outp + 1;
					} while (i < (int32_t)len);
					inptr = inp + 2;
					goto out;
				}
			}
			else {
				i = 0;
				if (0 < (int32_t)len) {
					do {
						i = i + 1;
						*outp = *inptr;
						outp = outp + 1;
					} while (i < (int32_t)len);
				}
			}
			inptr = inp + 2;
		}
		else {
			i = 0;
			if (len < 0xe0) {
				len = len - 0xbc;
				val1 = *inptr;
				inptr = inp + 3;
				/* outptr = outp + -(uint)CONCAT11(val1,inp[2]); */
				outptr = outp -((uint16_t)val1 << 8 | (uint8_t)inp[2]);
				if (0 < (int32_t)len) {
					do {
						val1 = *outptr;
						outptr = outptr + 1;
						i = i + 1;
						*outp = val1;
						outp = outp + 1;
					} while (i < (int32_t)len);
				}
			}
			else {
				if (len < 0xf0) {
					len = len - 0xdc;
					val1 = *inptr;
					val2 = inp[2];
					inptr = inp + 3;
					i = 0;
					if (0 < (int32_t)len) {
						do {
							*outp = val2;
							outp = outp + 1;
							i = i + 1;
							val2 = val2 + val1;
						} while (i < (int32_t)len);
					}
				}
			}
		}
out:
		inp = inptr;
		if (len == 0xff) {
			*ilen = inp - old_inp;
			*olen = outp - old_outp;
			return;
		}
	} while (1);
}

static void usage(const char *argv0)
{
	fprintf(stderr, "usage: %s <CDB file>\n", argv0);
}

int main(int argc, char *argv[])
{
	static uint8_t outbuf[0x200000];
	static char buf[4096];
	FILE *f;
	uint8_t *cdb;
	cdb_entry *entries;
	uint32_t zero;
	size_t offset;
	size_t ilen;
	size_t olen;
	size_t i;
	size_t j;
	int len;

	if (argc != 2)
	{
		usage(argv[0]);
		return 1;
	}

	cdb = read_file(argv[1], NULL);
	entries = (cdb_entry *)cdb;
	zero = 0;

	for (i = 0; entries[i].sector_offset || entries[i].sector_size; ++i) {
		for (j = 0, offset = 0;
		     offset < entries[i].sector_size * 0x800;
		     ++j, offset += ilen) {
			assert(offset + 4 < entries[i].sector_size * 0x800);
			if (!memcmp(&cdb[entries[i].sector_offset * 0x800 + offset],
				    &zero, sizeof(zero))) {
				fprintf(stderr, "Stop at offset %zu\n", offset);
				break;
			}
			cdb_decompress(&cdb[entries[i].sector_offset * 0x800 + offset],
				       outbuf, &ilen, &olen);
			assert(ilen);
			assert(olen);
			len = snprintf(buf, sizeof(buf), "%zu_%zu.bin", i, j);
			assert(len >= 0 && (size_t)len < sizeof(buf));
			f = fopen(buf, "wb");
			assert(f != NULL);
			assert(fwrite(outbuf, olen, 1, f) == 1);
			assert(!fclose(f));
		}
	}

	return 0;
}
