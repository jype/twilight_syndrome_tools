#!/bin/sh

usage() {
	printf 'usage: %s <CLUT binary> [<image binary>...]\n' "$0"
}

DIR="$(dirname "$(readlink -f "$0")")"
CLUT="$1"

[ -z "$CLUT" ] && { usage; exit 1; }

shift

for f in "$@"; do
	"$DIR/join_tim" "$f" "$CLUT" "${f%*.bin}.tim"
done
