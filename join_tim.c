#include <assert.h>
#include <stdint.h>
#include <stdio.h>

static void usage(const char *argv0)
{
	fprintf(stderr,
		"usage: %s <CLUT binary> <image binary> <TIM output file>\n",
		argv0);
}

int main(int argc, char *argv[])
{
	static char pixel_data[128];
	static char clut_data[512];
	FILE *pixel;
	FILE *clut;
	FILE *tim;
	uint32_t data;

	if (argc != 4) {
		usage(argv[0]);
		return 1;
	}

	pixel = fopen(argv[1], "rb");
	assert(pixel != NULL);
	assert(fread(pixel_data, sizeof(pixel_data), 1, pixel) == 1);
	assert(!fclose(pixel));

	clut = fopen(argv[2], "rb");
	assert(clut != NULL);
	assert(fread(clut_data, sizeof(clut_data), 1, clut) == 1);
	assert(!fclose(clut));

	tim = fopen(argv[3], "wb");
	assert(tim != NULL);

	/* ID */
	data = 0x00000010;
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* FLAG */
	data = (1 << 3);
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* CLUT bnum */
	data = sizeof(uint32_t) * 3 + sizeof(clut_data);
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* CLUT DY DX */
	data = 511 << 16;
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* CLUT H W */
	data = (1 << 16) | 256;
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* CLUT entries */
	assert(fwrite(clut_data, sizeof(clut_data), 1, tim) == 1);

	/* Pixel bnum */
	data = sizeof(uint32_t) * 3 + 16 * (16 / 2);
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* Pixel DY DX */
	data = 0;
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* Pixel H W */
	data = (16 << 16) | (16 / 4);
	assert(fwrite(&data, sizeof(data), 1, tim) == 1);

	/* Pixel data */
	assert(fwrite(pixel_data, sizeof(pixel_data), 1, tim) == 1);

	assert(!fclose(tim));

	return 0;
}
